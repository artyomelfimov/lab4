
public class TicTacToeModel {
	private boolean playerXTurn;
	private int[][] spaces;
	private CommandManager commandManager;
	public TicTacToeModel() {
		spaces = new int[3][3]; 
		playerXTurn = true;
		commandManager = new CommandManager();
	}

	public boolean isPlayerXTurn() {
		return playerXTurn;
	}

	public boolean isPlayerOTurn() {
		return !playerXTurn;
	}

	public void placeX(int row, int col) {
		assert(playerXTurn);
		assert(spaces[row][col] == 0);
		commandManager.executeCommand(new PlaceXCommand(this, row, col));
	}

	public void placeO(int row, int col) {
		assert(!playerXTurn);
		assert(spaces[row][col] == 0);
		commandManager.executeCommand(new PlaceOCommand(this, row, col));
	}
	

	public boolean isSpaceEmpty(int row, int col) {
		return (spaces[row][col] == 0);
	}
	

	public boolean isSpaceX(int row, int col) {
		return (spaces[row][col] == 1);
	}
	

	public boolean isSpaceO(int row, int col) {
		return (spaces[row][col] == 2);
	}
	

	public boolean hasPlayerXWon() {
		//Check rows
		if(spaces[0][0] == 1 && spaces[0][1] == 1 && spaces[0][2] == 1) return true;
		if(spaces[1][0] == 1 && spaces[1][1] == 1 && spaces[1][2] == 1) return true;
		if(spaces[2][0] == 1 && spaces[2][1] == 1 && spaces[2][2] == 1) return true;
		//Check columns
		if(spaces[0][0] == 1 && spaces[1][0] == 1 && spaces[2][0] == 1) return true;
		if(spaces[0][1] == 1 && spaces[1][1] == 1 && spaces[2][1] == 1) return true;
		if(spaces[0][2] == 1 && spaces[1][2] == 1 && spaces[2][2] == 1) return true;
		//Check diagonals
		if(spaces[0][0] == 1 && spaces[1][1] == 1 && spaces[2][2] == 1) return true;
		if(spaces[0][2] == 1 && spaces[1][1] == 1 && spaces[2][0] == 1) return true;
		//Otherwise, there is no line
		return false;
	}
	

	public boolean hasPlayerOWon() {
		//Check rows
		if(spaces[0][0] == 2 && spaces[0][1] == 2 && spaces[0][2] == 2) return true;
		if(spaces[1][0] == 2 && spaces[1][1] == 2 && spaces[1][2] == 2) return true;
		if(spaces[2][0] == 2 && spaces[2][1] == 2 && spaces[2][2] == 2) return true;
		//Check columns
		if(spaces[0][0] == 2 && spaces[1][0] == 2 && spaces[2][0] == 2) return true;
		if(spaces[0][1] == 2 && spaces[1][1] == 2 && spaces[2][1] == 2) return true;
		if(spaces[0][2] == 2 && spaces[1][2] == 2 && spaces[2][2] == 2) return true;
		//Check diagonals
		if(spaces[0][0] == 2 && spaces[1][1] == 2 && spaces[2][2] == 2) return true;
		if(spaces[0][2] == 2 && spaces[1][1] == 2 && spaces[2][0] == 2) return true;
		//Otherwise, there is no line
		return false;
	}
	

	public boolean isGameOver() {
		if(hasPlayerXWon() || hasPlayerOWon()) return true;
		for(int row = 0; row < 3; row++) {
			for(int col = 0; col < 3; col++) {
				if(spaces[row][col] == 0) return false;
			}
		}
		return true;
	}
	

	public boolean canUndo() {
		return commandManager.isUndoAvailable();
	}
	

	public void undo() {
		commandManager.undo();
	}


	
	

	private class PlaceXCommand implements Command {
		private TicTacToeModel model;
		private int previousValue;
		private boolean previousTurn;
		private int row;
		private int col;
		

		private PlaceXCommand(TicTacToeModel model, int row, int col) {
			this.model = model;
			this.row = row;
			this.col = col;
			this.previousValue = model.spaces[row][col];
			this.previousTurn = model.playerXTurn;
		}
		
		public void execute() {
			model.spaces[row][col] = 1;		
			model.playerXTurn = false;
		}
		
		public void undo() {
			model.spaces[row][col] = previousValue;
			model.playerXTurn = previousTurn;
		}		
	}

	private class PlaceOCommand implements Command {
		private TicTacToeModel model;
		private int previousValue;
		private boolean previousTurn;
		private int row;
		private int col;
		

		private PlaceOCommand(TicTacToeModel model, int row, int col) {
			this.model = model;
			this.row = row;
			this.col = col;
			this.previousValue = model.spaces[row][col];
			this.previousTurn = model.playerXTurn;
		}
		
		public void execute() {
			model.spaces[row][col] = 2;		
			model.playerXTurn = true;
		}
		
		public void undo() {
			model.spaces[row][col] = previousValue;
			model.playerXTurn = previousTurn;
		}		
	}	
	

}